## ON RASPBERRY PI (ArchLinux)
------------------

1. Create two files:
 - hook file:
 
    ```sh
    cat << EOF | sudo tee /etc/initcpio/hooks/overlayroot
    run_hook() {
        if [ "\${rwopt:-ro}" != 'rw' ]; then
                mount_handler="overlayfs_mount_handler"
        fi
    }
    overlayfs_mount_handler() {
        /bin/mkdir /ro
        /bin/mkdir /overlay
        mount -t tmpfs tmpfs -o rw /overlay
        /bin/mkdir /overlay/rwdir
        /bin/mkdir /overlay/workdir
        
        mount \${fstype:+-t \$fstype} -o \${rwopt:-ro}\${rootflags:+,\$rootflags} "\$root" /ro
        mount -t overlay overlay -o lowerdir=/ro,upperdir=/overlay/rwdir,workdir=/overlay/workdir "\$1"
        
        /bin/rm -rf "\$1"/var/log/journal
        
        [ -d "\$1/ro" ] || /bin/mkdir "\$1/ro"
        [ -d "\$1/overlay" ] || /bin/mkdir "\$1/overlay"
        mount --move /ro "\$1/ro"
        mount --move /overlay "\$1/overlay"
    }
    EOF
    ```
 - And install script for hook:
 
    ```sh
    cat << EOF | sudo tee /etc/initcpio/install/overlayroot
    #!/bin/bash
    build() {
        add_module overlay
        add_runscript
    }
    help() {
        cat <<HELPEOF
        OverlayFS root hook
    HELPEOF
    }
    EOF
    ```

1. Add overlay to mkinitcpio.conf
```sh
sudo sed -i 's/fsck"/overlayroot"/' /etc/mkinitcpio.conf
```

1. Generate initrd:
```sh
sudo mkinitcpio -p linux-raspberrypi
```


### Now if there is `rw` parameter in cmdline.txt then rootfs will be read-write, otherwise read-only and all changes will be applied to ramdisk